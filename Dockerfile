FROM openjdk:11 AS build
WORKDIR /app
COPY . .
RUN chmod +x mvnw && ./mvnw clean package

FROM openjdk:11
COPY --from=build /app/target/studentsystem*.jar /usr/local/lib/studentsystem.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/usr/local/lib/studentsystem.jar"]
